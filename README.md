# WEATHER FORECAST FOR LINUX MINT 

## A Weather Applet for Cinnamon and Xfce Desktop Environments

This  is another bash script that serves as a weather applet for Linux Mint OS.

It will detect the Desktop Environment (Cinnamon or Xfce), and will show the weather in the panel.

![image 1](screenshots/01.png)

It will work with the  CommandRunner applet (in the case of Cinnamon DE) or Generic Monitor applet(XFCE).
Hovering over the panel applet will show a tooltip with the following info:

* Last update time
* Current conditions
* Temperature
* Wind direction and speed
* View range
* Precipitation (rainfall) in mm

![image 2](screenshots/02.png)

Clicking on the applet will also give weather forecast for the 2 following days.

![image 3](screenshots/03.png)



## DEPENDENCIES
In order to work, this script depends on the powerful command **cURL**.

**The source of the data that are presented**:

 [wttr.in](wttr.in) 

[https://github.com/chubin/wttr.in](https://github.com/chubin/wttr.in)

The presentation of the forecast is done with the dialog command **yad**.

To install these dependencies, open a terminal and type:

`$ sudo apt install curl yad`

## INSTRUCTIONS

-  Change Directory to forecast_xfce_cinnamon

`$ cd forecast_xfce_cinnamon`

- **IMPORTANT**: Enter your **current location**
In order for the script to function , you must insert your location in  line 7 of the forecast_xfce_cinnamon.sh script.
you can do it with this  command in the terminal:

`sed -i '7i curl wttr.in/city'sname?FQT' Forecast.sh`

For example, if the location was Athens,Greece the command would be :

`sed -i '7i curl wttr.in/Athens,Greece?FQT' Forecast.sh`

You can find more info on setting the location, and on how this command works, by executing this command in the terminal:

`curl wttr.in/:help`

-  Make Forecast.sh executable with the following command in terminal:

`chmod +x Forecast.sh `

You can customize the dialog window geometry, by altering the **YAD_WINDOW_GEOMETRY** variable in line 11 of the script Forecast.sh.


For **Cinnamon DE**:

* Make sure you download the **CommandRunner Applet** in cinnamon panel:

 * Right click on the panel you want to present the network monitor

 * Select **Applets**

 * Select **Download** tab, select **CommandRunner** and download it

 * Back on the **Manage** tab, press the CommandRunner **Configure** button.

 * In the **Command** field, enter :
 /home/user'sname/path-to-directory/forecast_xfce_cinnamon/Forecast.sh
 * In the **Run Interval** field, put: 1800 seconds (refreshing every 30 minutes in my opinion is good enough)
 * Hit **Apply**
 * You are good to go.


For **Xfce DE**:

* **If the gnome icon theme is not installed in your system, add the full path to the forecast_xfce_cinnamon/png/ folder in line 13 of Forecast.sh.**

* Make sure you add the **Generic Monitor Applet** in the panel:

 * Right click on the panel you want to add the network monitor

 * Select **Panel**

 * Select **+Add New Items** tab, select **Generic Monitor** and press  **Add**.
 
 * Right-click on the Generic Monitor Applet that has appeared on the panel, select **Properties**.

 * In the **Command** field, enter : /home/user'sname/path-to-directory/forecast_xfce_cinnamon/Forecast.sh
 * Untick the Label box
 * In the **Period(s)** field, put: 1800 seconds (refreshing every 30 minutes in my opinion is good enough)
 * Close the Properties tab
 * You are good to go.





