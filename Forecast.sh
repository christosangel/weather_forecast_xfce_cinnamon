#! /bin/bash
#┏━╸┏━┓┏━┓┏━╸┏━╸┏━┓┏━┓╺┳╸
#┣╸ ┃ ┃┣┳┛┣╸ ┃  ┣━┫┗━┓ ┃
#╹  ┗━┛╹┗╸┗━╸┗━╸╹ ╹┗━┛ ╹
# A bash script written by Christos Angelopoulos, October 2021
#PLEASE INSERT YOUR LOCATION ON LINE 7. FOR MORE INFO, EXECUTE IN TERMINAL:  curl wttr.in/:help
curl -s wttr.in/~Athens,Greece?FQT  -o /tmp/Forecast.txt
# Insert expanation text to Forecast.txt
if [[ ! -d /tmp/Forecast_days ]];then mkdir /tmp/Forecast_days;fi
#GEOMETRY OF YAD DIALOG WINDOW:
YAD_WINDOW_GEOMETRY="600x400"
#if the gnome icon theme is not installed in your system, add here the full path to the forecast_xfce_cinnamon/png/
IMAGE_DIRECTORY="/usr/share/icons/gnome/24x24/status/"
#parse Forecast.txt and create forecast for today, 1d.txt
function presentation() {
yad --list --column="DAY" --column="MORNING" --column="NOON" --column="EVENING" --column="NIGHT" "$(cat /tmp/Forecast_days/DATE1.tx)"  "$(cat /tmp/Forecast_days/today1.txt)" "$(cat /tmp/Forecast_days/today2.txt)" "$(cat /tmp/Forecast_days/today3.txt)" "$(cat /tmp/Forecast_days/today4.txt)" "$(cat /tmp/Forecast_days/DATE2.tx)" "$(cat /tmp/Forecast_days/tomorrow1.txt)" "$(cat /tmp/Forecast_days/tomorrow2.txt)" "$(cat /tmp/Forecast_days/tomorrow3.txt)" "$(cat /tmp/Forecast_days/tomorrow4.txt)" "$(cat /tmp/Forecast_days/DATE3.tx)"   "$(cat /tmp/Forecast_days/day_after_tomorrow1.txt)" "$(cat /tmp/Forecast_days/day_after_tomorrow2.txt)" "$(cat /tmp/Forecast_days/day_after_tomorrow3.txt)" "$(cat /tmp/Forecast_days/day_after_tomorrow4.txt)" --geometry 600x400+400+50 --undecorated --no-buttons --skip-taskbar --close-on-unfocus --vscroll-policy=never --no-selection --grid-lines=both --no-rules-hint
}
function get_date() {
#insert Forecast.txt line no, destination file
	LINE=$(head -$1 /tmp/Forecast.txt|tail +$1);LINE=${LINE:57:12};echo "$LINE">$2
}
function get_data(){
# $1: Time of day, $2:start line of data. $3:stop line od data $4:first column of line to split data $5: destination file
#First echo $1 to file ie Morning, Noon etc
echo " ">/tmp/Forecast_days/$1.txt

FIRSTLINE=$2
LASTLINE=$3
while [ $FIRSTLINE -le $LASTLINE ]
do
 LINE=$(head -$FIRSTLINE /tmp/Forecast.txt|tail +$FIRSTLINE);LINE=${LINE:$4:15};echo "$LINE">>/tmp/Forecast_days/$1.txt
 ((FIRSTLINE++))
done
}

##################
## MAIN ROUTINE ##
##################
#dates
get_date 7 /tmp/Forecast_days/DATE1.tx
get_date 17 /tmp/Forecast_days/DATE2.tx
get_date 27 /tmp/Forecast_days/DATE3.tx
#today
get_data today1 10 14 16
get_data today2 10 14 47
get_data today3 10 14 78
get_data today4 10 14 109

#tomorrow
get_data tomorrow1 20 24 16
get_data tomorrow2 20 24 47
get_data tomorrow3 20 24 78
get_data tomorrow4 20 24 109

#day_after_tomorrow
get_data day_after_tomorrow1 30 34 16
get_data day_after_tomorrow2 30 34 47
get_data day_after_tomorrow3 30 34 78
get_data day_after_tomorrow4 30 34 109

sed -i 's/\s* $//g' /tmp/Forecast_days/*.txt
# Set arrays and dictionary
declare -A PAIR_ICON
declare -A PAIR_CHAR
STATUS=("Sunny" \
								"Clear" \
								"Cloudy" \
								"Partly cloudy" \
								"Overcast" \
								"Patchy light d…" \
								"Patchy light dri" \
								"Patchy light r…" \
								"Patchy rain" \
								"Light drizzle" \
								"Light rain" \
								"Light rain sho…" \
								"Drizzle" \
								"Shower" \
        "Rain" \
								"Moderate rain" \
								"Moderate rain …" \
								"Patchy light drizzle" \
								"Patchy rain po…" \
								"Thundery outbr..." \
								"Mist" \
								"Fog" \
								"Light snow" \
								"Moderate snow" \
								"Heavy snow" \
								"Snow")
 ICON=("weather-clear" \
 "weather-clear" \
 "weather-few-clouds" \
 "weather-few-clouds" \
 "weather-overcast" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers-scattered" \
 "weather-showers" \
 "weather-showers" \
 "weather-storm" \
 "weather-fog" \
 "weather-fog" \
 "weather-snow" \
 "weather-snow" \
 "weather-snow" \
 "weather-snow")
CHARACTER=("☀️" "☀️" "☁️" "⛅️" "☁️" "🌦️" "🌦️" "🌦️" "🌦️" "🌧️" "🌧️" "🌦️" "🌧️" "🌧️" "🌧️" "🌧️" "🌧️" "🌧️" "🌧️" "⛈️" "🌫️" "🌫️" "🌨️" "🌨️" "🌨️" "🌨️")
i=0
iMAX=$(( ${#STATUS[@]} - 1 ))
while [ $i -le $iMAX ]
do
 STAT=${STATUS[$i]}
 #echo "STAT = ""$STAT"
 IC=${ICON[$i]}

 CHAR=${CHARACTER[$i]}

	PAIR_CHAR[$STAT]="$CHAR"
	PAIR_ICON["$STAT"]="$IC"
	#echo "PAIR_ICON[""$STAT""]="${PAIR_ICON[$STAT]}
	#echo "PAIR_CHAR[""$STAT""]="${PAIR_CHAR[$STAT]}
	((i++))
done
for TIMEFILE in /tmp/Forecast_days/*.txt
do
	CURRENT_STATUS=$(head -2 $TIMEFILE|tail +2)
	#echo "$CURRENT_STATUS"
	#resolve unpaired status icons
	s=0;sMAX=$(( ${#STATUS[@]} - 1 ))
	while [ $s -le $sMAX ]
	do
		if [[ "$CURRENT_STATUS" == *"${STATUS[$s]}"* ]]
		then
			CURRENT_STATUS="${STATUS[$s]}"
			break
		fi
		((s++))
	done
	CURRENT_ICON=${PAIR_ICON[$CURRENT_STATUS]}
	#echo "$CURRENT_ICON"

	CURRENT_CHAR=${PAIR_CHAR[$CURRENT_STATUS]}
	#echo "$TIMEFILE""  :  " "$CURRENT_STATUS" "  :  " "$CURRENT_CHAR"
	TEMPSTR=$CURRENT_CHAR"$(cat $TIMEFILE)"
	echo "$TEMPSTR">$TIMEFILE
	#CURRENT_TEXT=$CURRENT_CHAR"$(cat $TIMEFILE)"
done

#make Forecast_panel.txt
sed -i '1,5s/................//;2s/(.*)//;2s/+//' /tmp/Forecast.txt

TEMP=$(head -2 /tmp/Forecast.txt|tail +2);TEMP=${TEMP:0:5}
sed -i '1s/\s* $//g' /tmp/Forecast.txt
CURRENT_STATUS=$(head -1 /tmp/Forecast.txt);CURRENT_STATUS=${CURRENT_STATUS:0:16}
#echo "CURRENT_STATUS : ""$CURRENT_STATUS"


#resolve unpaired status icons
s=0;sMAX=$(( ${#STATUS[@]} - 1 ))
while [ $s -le $sMAX ]
do
	if [[ "$CURRENT_STATUS" == *"${STATUS[$s]}"* ]]
	then
		CURRENT_STATUS="${STATUS[$s]}"
		break
	fi
	((s++))
done


#echo "CURRENTSTATUS : "$CURRENT_STATUS
IMG=${PAIR_ICON[$CURRENT_STATUS]}
#echo "IMG : "$IMG
if [ $(date +%H) -le 5 ]|| [ $(date +%H) -ge 19 ]
then
 if [[ $IMG == "weather-clear" ]] ||[[ $IMG == "weather-few-clouds" ]]
 then
  IMG=$IMG"-night"
 fi
fi
TOOLTIP="Weather Forecast
────────────────────
Updated      :          $(date +%H:%M)
────────────────────
Condition    :    $(head -1 /tmp/Forecast.txt|tail +1)
Temperature  :  $(head -2 /tmp/Forecast.txt|tail +2)
Wind         :      $(head -3 /tmp/Forecast.txt|tail +3)
View range   :   $(head -4 /tmp/Forecast.txt|tail +4)
Precipitation: $(head -5 /tmp/Forecast.txt|tail +5)"


#Detect desktop environment: xfce or cinnamon
if [ $DESKTOP_SESSION = "cinnamon" ]
then
	echo -e "<xml>
	<appsettings><tooltip>
	"$TOOLTIP"
	</tooltip>

	<clickaction>yad --list --title='Forecast' --column='DAY' --column='MORNING' --column='NOON' --column='EVENING' --column='NIGHT' '$(cat /tmp/Forecast_days/DATE1.tx)'  '$(cat /tmp/Forecast_days/today1.txt)' '$(cat /tmp/Forecast_days/today2.txt)' '$(cat /tmp/Forecast_days/today3.txt)' '$(cat /tmp/Forecast_days/today4.txt)' '$(cat /tmp/Forecast_days/DATE2.tx)' '$(cat /tmp/Forecast_days/tomorrow1.txt)' '$(cat /tmp/Forecast_days/tomorrow2.txt)' '$(cat /tmp/Forecast_days/tomorrow3.txt)' '$(cat /tmp/Forecast_days/tomorrow4.txt)' '$(cat /tmp/Forecast_days/DATE3.tx)'   '$(cat /tmp/Forecast_days/day_after_tomorrow1.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow2.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow3.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow4.txt)' --geometry $YAD_WINDOW_GEOMETRY --center  --no-buttons --skip-taskbar --close-on-unfocus --vscroll-policy=never --no-selection --grid-lines=both --no-rules-hint</clickaction>
	</appsettings>

	<item>
	<type>icon</type>
		<value>""$IMAGE_DIRECTORY""$IMG"".png</value>
		<attr>
			<style>icon-size: 2.4em;</style>
		</attr>
	</item>
	<item>
		<type>text</type>
		<attr>
			<style>font-size: 10pt;font-weight: bold;color: silver</style>
		</attr>
		<value>"$TEMP"</value>
		</item>

</xml>"
else
	#On EVERY other $DESKTOP_SESSION including xfce xubuntu etc using genmon
	echo  "<txt>$TEMP</txt>
	<img>""$IMAGE_DIRECTORY""$IMG"".png</img>
	<click>yad --list --title='Forecast' --column='DAY' --column='MORNING' --column='NOON' --column='EVENING' --column='NIGHT' '$(cat /tmp/Forecast_days/DATE1.tx)'  '$(cat /tmp/Forecast_days/today1.txt)' '$(cat /tmp/Forecast_days/today2.txt)' '$(cat /tmp/Forecast_days/today3.txt)' '$(cat /tmp/Forecast_days/today4.txt)' '$(cat /tmp/Forecast_days/DATE2.tx)' '$(cat /tmp/Forecast_days/tomorrow1.txt)' '$(cat /tmp/Forecast_days/tomorrow2.txt)' '$(cat /tmp/Forecast_days/tomorrow3.txt)' '$(cat /tmp/Forecast_days/tomorrow4.txt)' '$(cat /tmp/Forecast_days/DATE3.tx)'   '$(cat /tmp/Forecast_days/day_after_tomorrow1.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow2.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow3.txt)' '$(cat /tmp/Forecast_days/day_after_tomorrow4.txt)' --geometry $YAD_WINDOW_GEOMETRY --center  --no-buttons --skip-taskbar --close-on-unfocus --vscroll-policy=never --no-selection --grid-lines=both --no-rules-hint</click>
	<tool>
	$TOOLTIP</tool>"
fi
